USE [master]
GO
/****** Object:  Database [Juego]    Script Date: 13/9/2021 21:51:26 ******/
CREATE DATABASE [Juego]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Juego', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\Juego.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Juego_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\Juego_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [Juego] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Juego].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Juego] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Juego] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Juego] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Juego] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Juego] SET ARITHABORT OFF 
GO
ALTER DATABASE [Juego] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Juego] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Juego] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Juego] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Juego] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Juego] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Juego] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Juego] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Juego] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Juego] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Juego] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Juego] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Juego] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Juego] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Juego] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Juego] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Juego] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Juego] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Juego] SET  MULTI_USER 
GO
ALTER DATABASE [Juego] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Juego] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Juego] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Juego] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Juego] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [Juego] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [Juego] SET QUERY_STORE = OFF
GO
USE [Juego]
GO
/****** Object:  Table [dbo].[personaje]    Script Date: 13/9/2021 21:51:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[personaje](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FUERZA] [int] NOT NULL,
	[NIVEL] [int] NOT NULL,
	[PUNTOS] [int] NOT NULL,
	[EXPERIENCIA] [int] NOT NULL,
	[ID_RAZA] [int] NOT NULL,
 CONSTRAINT [PK_personaje] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TABLA_RAZA]    Script Date: 13/9/2021 21:51:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TABLA_RAZA](
	[ID_RAZA] [int] IDENTITY(1,1) NOT NULL,
	[Tipo_Raza] [varchar](50) NOT NULL,
 CONSTRAINT [PK_TABLA_RAZA] PRIMARY KEY CLUSTERED 
(
	[ID_RAZA] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[VISTA_JUGADOR]    Script Date: 13/9/2021 21:51:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VISTA_JUGADOR]
AS
SELECT        p.ID_PERSONAJE, p.FUERZA, p.NIVEL, p.PUNTOS, p.EXPERIENCIA, p.ID_RAZA, p.ID_HABILIDADES, r.Tipo_Raza
FROM            dbo.personaje AS p INNER JOIN
                         dbo.TABLA_RAZA AS r ON p.ID_RAZA = r.ID_RAZA
GO
/****** Object:  Table [dbo].[RELACIONES]    Script Date: 13/9/2021 21:51:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RELACIONES](
	[ID] [int] NOT NULL,
	[ID_HABILIDAD] [int] NOT NULL,
 CONSTRAINT [PK_RELACIONES] PRIMARY KEY CLUSTERED 
(
	[ID] ASC,
	[ID_HABILIDAD] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TABLA_HABILIDAD]    Script Date: 13/9/2021 21:51:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TABLA_HABILIDAD](
	[ID_HABILIDAD] [int] IDENTITY(1,1) NOT NULL,
	[Tipo_Habilidad] [varchar](50) NOT NULL,
 CONSTRAINT [PK_TABLA_HABILIDAD] PRIMARY KEY CLUSTERED 
(
	[ID_HABILIDAD] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[Vista_Habilidad]    Script Date: 13/9/2021 21:51:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vista_Habilidad]
AS
SELECT        dbo.RELACIONES.ID, dbo.TABLA_HABILIDAD.ID_HABILIDAD AS Expr1, dbo.TABLA_HABILIDAD.Tipo_Habilidad
FROM            dbo.RELACIONES INNER JOIN
                         dbo.TABLA_HABILIDAD ON dbo.RELACIONES.ID_HABILIDAD = dbo.TABLA_HABILIDAD.ID_HABILIDAD
GO
ALTER TABLE [dbo].[personaje]  WITH CHECK ADD  CONSTRAINT [FK_personaje_TABLA_RAZA1] FOREIGN KEY([ID_RAZA])
REFERENCES [dbo].[TABLA_RAZA] ([ID_RAZA])
GO
ALTER TABLE [dbo].[personaje] CHECK CONSTRAINT [FK_personaje_TABLA_RAZA1]
GO
ALTER TABLE [dbo].[RELACIONES]  WITH CHECK ADD  CONSTRAINT [FK_RELACIONES_personaje] FOREIGN KEY([ID])
REFERENCES [dbo].[personaje] ([ID])
GO
ALTER TABLE [dbo].[RELACIONES] CHECK CONSTRAINT [FK_RELACIONES_personaje]
GO
ALTER TABLE [dbo].[RELACIONES]  WITH CHECK ADD  CONSTRAINT [FK_RELACIONES_TABLA_HABILIDAD] FOREIGN KEY([ID_HABILIDAD])
REFERENCES [dbo].[TABLA_HABILIDAD] ([ID_HABILIDAD])
GO
ALTER TABLE [dbo].[RELACIONES] CHECK CONSTRAINT [FK_RELACIONES_TABLA_HABILIDAD]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[78] 4[7] 2[7] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "RELACIONES"
            Begin Extent = 
               Top = 72
               Left = 367
               Bottom = 168
               Right = 537
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "TABLA_HABILIDAD"
            Begin Extent = 
               Top = 26
               Left = 103
               Bottom = 122
               Right = 273
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vista_Habilidad'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vista_Habilidad'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[78] 4[7] 2[7] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "p"
            Begin Extent = 
               Top = 36
               Left = 24
               Bottom = 166
               Right = 201
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "r"
            Begin Extent = 
               Top = 29
               Left = 351
               Bottom = 120
               Right = 521
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VISTA_JUGADOR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VISTA_JUGADOR'
GO
USE [master]
GO
ALTER DATABASE [Juego] SET  READ_WRITE 
GO
