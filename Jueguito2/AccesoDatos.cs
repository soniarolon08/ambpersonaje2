﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Jueguito2
{
    class AccesoDatos
    {
        SqlConnection conexion = new SqlConnection();


        public AccesoDatos()
        {

        }

        public void Abrir()
        {
            conexion.ConnectionString = @"Data source=.\sqlexpress; Initial Catalog=Juego; Integrated Security= SSPI";
            conexion.Open();
        }

        public void Cerrar()
        {
            conexion.Close();
            GC.Collect();
        }

        private SqlCommand CrearComando(string sql)
        {
            SqlCommand comando = new SqlCommand();
            comando.CommandType = CommandType.Text;
            comando.CommandText = sql;
            comando.Connection = conexion;

            return comando;
        }

        // ---------------- LISTAMOS RAZAS ------------------- //

        public List<RAZA> ListarRazas()
        {
            List<RAZA> razas = new List<RAZA>();

            Abrir();

            string sql = "Select * from tabla_raza";

            SqlCommand comando = CrearComando(sql);

            SqlDataReader lector = comando.ExecuteReader();

            while (lector.Read())
            {
                RAZA raza = new RAZA();
                raza.Id_Raza = int.Parse(lector["id_raza"].ToString());
                raza.Tipo_Raza = lector["tipo_raza"].ToString();

                razas.Add(raza);
            }

            lector.Close();
            Cerrar();
            return razas;
        }


        // ---------------- LISTAMOS HABILIDADES ------------------- //
        public List<HABILIDAD> ListarHabilidad()
        {
            List<HABILIDAD> listadohabilidades = new List<HABILIDAD>();
            Abrir();

            string sql = "Select * from tabla_habilidad";
            SqlCommand comando = CrearComando(sql);

            SqlDataReader lector = comando.ExecuteReader();

            while (lector.Read())
            {
                HABILIDAD informacionhabilidad = new HABILIDAD();
                informacionhabilidad.Id_Habilidad = int.Parse(lector["id_habilidad"].ToString()); 
                informacionhabilidad.Tipo_Habilidad = lector["tipo_habilidad"].ToString();

                listadohabilidades.Add(informacionhabilidad);
            }

            lector.Close();

            Cerrar();

            return listadohabilidades;
        }

        // ---------------- LISTAMOS PERSONAJE ---------------- //

        public List<PERSONAJE> ListarPersonajes()
        {
            List<PERSONAJE> informacionpersonajes = new List<PERSONAJE>();

            List<RAZA> informacionraza = ListarRazas();

            List<HABILIDAD> informacionhabilidades = ListarHabilidad();


            Abrir();

            string sql = "select * from personaje";
            SqlCommand comando = CrearComando(sql);
            SqlDataReader lector = comando.ExecuteReader();


            while (lector.Read())
            {
                PERSONAJE informacionnuevapersonaje = new PERSONAJE();

                informacionnuevapersonaje.Id = int.Parse(lector["id"].ToString());
                informacionnuevapersonaje.Fuerza = int.Parse(lector["fuerza"].ToString());
                informacionnuevapersonaje.Nivel = int.Parse(lector["nivel"].ToString());
                informacionnuevapersonaje.Puntos = int.Parse(lector["puntos"].ToString());
                informacionnuevapersonaje.Experiencia = int.Parse(lector["experiencia"].ToString());
                informacionnuevapersonaje.Id_Raza = int.Parse(lector["id_raza"].ToString());
                


                foreach (RAZA r in informacionraza) // El foreach se usa para recorrer todas las listas de razas que tenemos 
                {
                    if (lector.GetInt32(5) == r.Id_Raza) // El id_raza es el 5 en la tabla porque se empieza desde el 0
                    {
                        informacionnuevapersonaje.Tipo_Raza = r;
                    }
                }
                informacionpersonajes.Add(informacionnuevapersonaje);
            }

            lector.Close();
            Cerrar();
            return informacionpersonajes;

        }

        public int Escribir(string sql)
        {
            int filasAfectadas = 0;
            Abrir();
            SqlCommand comando = CrearComando(sql);

            try
            {
                filasAfectadas = comando.ExecuteNonQuery(); // Ejecuta el comando
            }
            catch (SqlException ex) // Para que sirve ex ? ya explico el profe
            {
                filasAfectadas = -1;
            }

            Cerrar();

            return filasAfectadas;  // Dependiendo de lo que vayamos a hacer, si tantas filas afectadas se hizo bien o no

        }

        public DataTable Leer(string sql)
        {
            Abrir();
            DataTable tabla= new DataTable(); // Esta tabla va a estar en memoria

            SqlDataAdapter adaptador = new SqlDataAdapter();
            adaptador.SelectCommand = CrearComando(sql);
            adaptador.Fill(tabla); // Esto en funcion a la consulta sql te llena todos los registros

            Cerrar();
            return tabla;

        }
    }

}
