﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace Jueguito2
{
    class HABILIDAD
    {
        private int id_habilidad;

        public int Id_Habilidad
        {
            get { return id_habilidad; }
            set { id_habilidad = value; }
        }

        private string tipo_habilidad;

        public string Tipo_Habilidad
        {
            get { return tipo_habilidad; }
            set { tipo_habilidad = value; }
        }

        public override string ToString()
        {
            return tipo_habilidad;
        }

    }
}
